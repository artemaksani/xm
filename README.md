# XM Demo Project

Selenide and Chrome Web Driver are used in project

## Installation

Clone Repository git clone https://artemaksani@bitbucket.org/artemaksani/xm.git

To run the project you will need to download a ChromeDriver for you operation system and put it into the root folder

https://chromedriver.storage.googleapis.com/index.html?path=73.0.3683.20/

## Usage

To run test go to xm/src/test/java/demo/functionalTests/CreateNewPartnerTest and click run in your IDE

To run with Maven use command mvn clean test

## Contributing
Pull requests and comments are welcome. 

