package demo.functionalTests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.github.javafaker.Faker;
import demo.pages.partners.AccountRegistrationPage;
import demo.pages.partners.HomePage;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.open;
import static drivers.ChromeDriver.initChromeDriver;

public class BasePartnerRegisterTest
{
    /*Better approach is to remove to property files all the variables*/

    private Faker faker = new Faker();
    private HomePage partnerHomePage = new HomePage();
    private demo.pages.xm.HomePage
            xmHomePage = new demo.pages.xm.HomePage();
    private AccountRegistrationPage
            accountRegistrationPage = new AccountRegistrationPage();
    private String accountPassword = "GlobalLogin101";
    private String baseUrl = "https://www.xm.com";
    private String phoneNumber = "3455675432";
    private String email = faker.lastName() + "@gmail.com";
    private String name = "Test" + faker.zipCode();
    private int randomNumber = (int) (Math.random() * 10 + 2);

    /*method will fill all the fields for a new partner*/
    public BasePartnerRegisterTest addPersonalDetails()
    {
        accountRegistrationPage.getFirstNameTxt().sendKeys(name);
        accountRegistrationPage.getLastName().sendKeys(name);
        accountRegistrationPage.getDateOfBirthDaySlct().selectOption(randomNumber);
        accountRegistrationPage.getDateOfBirthMonthSlct().selectOption(randomNumber);
        accountRegistrationPage.getDateOfBirthYearSlct().selectOption(randomNumber);
        accountRegistrationPage.getCountryOfResidenceSlct().selectOption(randomNumber);
        accountRegistrationPage.getCityTxt().sendKeys(faker.cityPrefix());
        accountRegistrationPage.getStateTxt().sendKeys(faker.stateAbbr());
        accountRegistrationPage.getResidentialAddressTxt().sendKeys(faker.streetAddress(false));
        accountRegistrationPage.getStreetNumberTxt().sendKeys(faker.zipCode());
        accountRegistrationPage.getPostalZipCodeTxt().sendKeys(faker.zipCode());
        accountRegistrationPage.getPhoneTxt().sendKeys(phoneNumber);
        accountRegistrationPage.getEmailTxt().sendKeys(email);
        accountRegistrationPage.getPreferredLanguageSlct().selectOption(randomNumber);
        accountRegistrationPage.getAccountPasswordTxt().sendKeys(accountPassword);
        accountRegistrationPage.getPasswordConfirmationTxt().sendKeys(accountPassword);
        return this;
    }

    /*method will check Terms and Condition checkbox with timeout*/
    public BasePartnerRegisterTest checkTermsAndConditions()
    {
        accountRegistrationPage.getTermsConditionsChckBx().click(3, 4);
        return this;
    }

    /*method will navigate to the create new partner page and check titles of pages on the way*/
    public BasePartnerRegisterTest navigateToPartnerRegisterPage()
    {
        initChromeDriver();
        open(baseUrl);
        xmHomePage.getTitle()
                .shouldHave(attribute
                        ("text", "Forex & CFD Trading on Stocks, " +
                                "Indices, Oil, Gold by XM™"));
        xmHomePage.getContinueBtn().click();
        xmHomePage.getPartnershipsLnk().click();
        partnerHomePage.getTitle()
                .shouldHave(attribute
                        ("text", "Forex Affiliate Program | Forex Affiliates | XM Partners"));
        partnerHomePage.getRegisterPartnerAccountBtn().click();
        accountRegistrationPage.getTitle()
                .shouldHave(attribute
                        ("text", "XM Partner Account Registration"));
        return this;
    }

    /*method will verify error message for Terms and Conditions*/
    public BasePartnerRegisterTest verifyErrorMessageDisplayed()
    {
        accountRegistrationPage.getErrorMessage().shouldBe(Condition.visible);
        return this;
    }

    /*method to Submit new partner*/
    public BasePartnerRegisterTest submit()
    {
        Selenide.executeJavaScript("window.scrollBy(0,250)", "");
        accountRegistrationPage.getOpenPartnerAccountBtn().click(3, 4);
        return this;
    }

    /*method to check success message for new partner*/
    public BasePartnerRegisterTest verifySuccessMessageDisplayed()
    {
        accountRegistrationPage.getAlertSuccess().shouldBe(Condition.appear);
        return this;
    }
}
