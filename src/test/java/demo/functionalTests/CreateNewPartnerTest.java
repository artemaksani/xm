package demo.functionalTests;

import org.testng.annotations.Test;

public class CreateNewPartnerTest extends BasePartnerRegisterTest
{
    @Test
    /*test will create a new partner, check error message for Terms and Conditions,
    check Success message for new Partner*/
    public void testCreatePartner()
    {
        navigateToPartnerRegisterPage()
                .addPersonalDetails()
                .submit()
                .verifyErrorMessageDisplayed()
                .checkTermsAndConditions()
                .submit()
                .verifySuccessMessageDisplayed();
    }
}
