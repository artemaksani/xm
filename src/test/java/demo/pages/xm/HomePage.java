package demo.pages.xm;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class HomePage
{
    /*Class to initialize all webelements at Home Page
and provide getters for them*/
    private SelenideElement partnershipsLnk;
    private SelenideElement continueBtn;
    private SelenideElement title;

    public HomePage()
    {
        partnershipsLnk = $(By.xpath("//a[@href='https://partners.xm.com']"));
        continueBtn = $(By.xpath("//button[@class='btn btn-red btn-solid btn-block " +
                "js-acceptDefaultCookie gtm-acceptDefaultCookieFirstVisit']"));
        title = $(By.xpath("//title"));
    }

    public SelenideElement getPartnershipsLnk()
    {
        return partnershipsLnk;
    }

    public SelenideElement getContinueBtn()
    {
        return continueBtn;
    }

    public SelenideElement getTitle()
    {
        return title;
    }
}
