package demo.pages.partners;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/*Class to initialize all webelements at Account Registration Page
and provide getters for them*/
public class AccountRegistrationPage
{
    private SelenideElement firstNameTxt;
    private SelenideElement lastName;
    private SelenideElement dateOfBirthDaySlct;
    private SelenideElement dateOfBirthMonthSlct;
    private SelenideElement dateOfBirthYearSlct;
    private SelenideElement countryOfResidenceSlct;
    private SelenideElement cityTxt;
    private SelenideElement stateTxt;
    private SelenideElement residentialAddressTxt;
    private SelenideElement streetNumberTxt;
    private SelenideElement postalZipCodeTxt;
    private SelenideElement phoneTxt;
    private SelenideElement emailTxt;
    private SelenideElement preferredLanguageSlct;
    private SelenideElement accountPasswordTxt;
    private SelenideElement passwordConfirmationTxt;
    private SelenideElement termsConditionsChckBx;
    private SelenideElement openPartnerAccountBtn;
    private SelenideElement errorMessage;
    private SelenideElement alertSuccess;
    private SelenideElement title;

    public AccountRegistrationPage()
    {
        firstNameTxt = $(By.id("first_name"));
        lastName = $(By.id("last_name"));
        dateOfBirthDaySlct = $(By.id("dob_day"));
        dateOfBirthMonthSlct = $(By.id("dob_month"));
        dateOfBirthYearSlct = $(By.id("dob_year"));
        countryOfResidenceSlct = $(By.id("country"));
        cityTxt = $(By.id("city"));
        stateTxt = $(By.id("state_region"));
        residentialAddressTxt = $(By.id("street_name"));
        streetNumberTxt = $(By.id("street_number"));
        postalZipCodeTxt = $(By.id("postal_zip"));
        phoneTxt = $(By.id("phone_number"));
        emailTxt = $(By.id("email"));
        preferredLanguageSlct = $(By.id("preferred_language"));
        accountPasswordTxt = $(By.id("account_password"));
        passwordConfirmationTxt = $(By.id("account_password_confirmation"));
        termsConditionsChckBx = $(By.xpath("//label[@for='agree_terms']"));
        openPartnerAccountBtn = $(By.xpath("//button[contains(text(),'Open Partner Account')]"));
        errorMessage = $(By.xpath("//label[contains(text(),'To register a Real Account, " +
                "you must accept all applicable Terms and Conditions')]"));
        alertSuccess = $(By.xpath("//div[@class='alert alert-success']"));
        title = $(By.xpath("//title"));
    }

    public SelenideElement getFirstNameTxt()
    {
        return firstNameTxt;
    }

    public SelenideElement getLastName()
    {
        return lastName;
    }

    public SelenideElement getDateOfBirthDaySlct()
    {
        return dateOfBirthDaySlct;
    }

    public SelenideElement getDateOfBirthMonthSlct()
    {
        return dateOfBirthMonthSlct;
    }

    public SelenideElement getDateOfBirthYearSlct()
    {
        return dateOfBirthYearSlct;
    }

    public SelenideElement getCountryOfResidenceSlct()
    {
        return countryOfResidenceSlct;
    }

    public SelenideElement getCityTxt()
    {
        return cityTxt;
    }

    public SelenideElement getStateTxt()
    {
        return stateTxt;
    }

    public SelenideElement getResidentialAddressTxt()
    {
        return residentialAddressTxt;
    }

    public SelenideElement getStreetNumberTxt()
    {
        return streetNumberTxt;
    }

    public SelenideElement getPostalZipCodeTxt()
    {
        return postalZipCodeTxt;
    }

    public SelenideElement getPhoneTxt()
    {
        return phoneTxt;
    }

    public SelenideElement getEmailTxt()
    {
        return emailTxt;
    }

    public SelenideElement getPreferredLanguageSlct()
    {
        return preferredLanguageSlct;
    }

    public SelenideElement getAccountPasswordTxt()
    {
        return accountPasswordTxt;
    }

    public SelenideElement getPasswordConfirmationTxt()
    {
        return passwordConfirmationTxt;
    }

    public SelenideElement getTermsConditionsChckBx()
    {
        return termsConditionsChckBx;
    }

    public SelenideElement getOpenPartnerAccountBtn()
    {
        return openPartnerAccountBtn;
    }

    public SelenideElement getErrorMessage()
    {
        return errorMessage;
    }

    public SelenideElement getAlertSuccess()
    {
        return alertSuccess;
    }

    public SelenideElement getTitle()
    {
        return title;
    }
}
