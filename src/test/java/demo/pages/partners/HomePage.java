package demo.pages.partners;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class HomePage
    /*Class to initialize all webelements at Home Page
and provide getters for them*/
{
    private SelenideElement registerPartnerAccountBtn;
    private SelenideElement title;

    public HomePage()
    {
        registerPartnerAccountBtn = $(By.xpath("//a[@href='https://partners.xm.com/registration']"));
        title = $(By.xpath("//title"));
    }

    public SelenideElement getRegisterPartnerAccountBtn()
    {
        return registerPartnerAccountBtn;
    }

    public SelenideElement getTitle()
    {
        return title;
    }
}
