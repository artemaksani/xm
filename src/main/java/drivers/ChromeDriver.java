package drivers;

public class ChromeDriver
{
    public static void initChromeDriver()
    {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        /*Define here path to your chromedriver binary*/
        System.setProperty("selenide.browser", "Chrome");
    }
}
